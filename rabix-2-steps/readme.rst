This test reproduces rabix failing to execute the second step of a workflow.


``rabix ../tools/touch.cwl jobs/touch.yml``

``rabix ../tools/cat.cwl jobs/cat.yml``

``rabix fails.cwl jobs/touch.yml``
