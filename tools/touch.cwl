cwlVersion: v1.0
class: CommandLineTool
baseCommand: [touch]
inputs:
  file_name:
    type: string
    inputBinding:
      position: 1
outputs:
  file:
    type: File
    outputBinding:
      glob: $(inputs.file_name)

