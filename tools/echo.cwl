cwlVersion: v1.0
class: CommandLineTool
baseCommand: [echo]
inputs:
  string:
    type: string
    inputBinding:
      position: 1
outputs:
  result:
    type: stdout

