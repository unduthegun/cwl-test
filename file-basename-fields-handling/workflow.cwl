cwlVersion: v1.0
class: Workflow

requirements:
  - class: StepInputExpressionRequirement

inputs:
  tool: File

outputs:
  rootFile:
    type: File
    outputSource: root/result
  extFile:
    type: File
    outputSource: ext/result

steps:
  root:
    run: ../tools/echo.cwl
    in:
      tool: tool
      string:
        valueFrom: $(inputs.tool.nameroot)
    out: [result]
  ext:
    run: ../tools/echo.cwl
    in:
      tool: tool
      string:
        valueFrom: $(inputs.tool.nameext)
    out: [result]

