To test there are 3 yml files.
One to test that the tool works, another to test that the workflow can work with the ``nameroot`` and ``nameext`` fields set;
and a last one that shows that the ``nameroot`` and ``nameext`` fields are not set automagically.

``cwltool ../tools/echo.cwl jobs/echo.yml``

``cwltool workflow.cwl jobs/fields-set-works.yml``

``cwltool workflow.cwl jobs/no-fields-fails.yml``
