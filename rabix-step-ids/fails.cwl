cwlVersion: v1.0
class: Workflow

inputs:
  file_name: string

outputs:
  file:
    type: File
    outputSource: ca>at/file

steps:
  touch:
    run: ../tools/touch.cwl
    in:
      file_name: file_name
    out: [file]
  cat>:
    run: ../tools/cat.cwl
    in:
      file: touch/file
    out: [file]
  ca>at:
    run: ../tools/cat.cwl
    in:
      file: touch/file
    out: [file]

