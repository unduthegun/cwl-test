Rabix 1.0.0-RC3 Fails to execute a workflows because of the characters in the step ids.

When a step id in the workflow contains the character `>` Rabix fails to write the output to the correct folder.

CWL is unclear about what characters are valid in an id,
but the reference implementation runs this workflow just fine.


``rabix ../tools/touch.cwl jobs/touch.yml``

``rabix ../tools/cat.cwl jobs/cat.yml``

``rabix fails.cwl jobs/touch.yml``
